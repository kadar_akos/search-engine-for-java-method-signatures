from sklearn.externals import joblib
from sklearn import linear_model
from sklearn import cross_decomposition
from sklearn import gaussian_process
from sklearn import tree
import numpy
import pickle
import os
import climate
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score

climate.enable_default_logging()

#from sklearn import preprocessing

def getDataset():
    path = os.getcwd()
    description_vectors = numpy.load(path+'/network/train_desps_features.npy')
    signature_vectors = numpy.load(path+'/network/train_signatures_features.npy')
    num_descs = len(description_vectors[1])
    return description_vectors, signature_vectors, num_descs

def getValidDataset():
    path = os.getcwd()
    description_vectors = numpy.load(path+'/network/valid_desps_features.npy')
    signature_vectors = numpy.load(path+'/network/valid_signatures_features.npy')
    return description_vectors, signature_vectors

def trainNetwork():

    path = os.getcwd()
    description_vectors, signature_vectors, num_descs = getDataset()
    arguments_in = open('arguments.txt')
    arguments = pickle.load(arguments_in)
    arguments['num_descs']= num_descs
    arguments_in.close()
    arguments_out = open('arguments.txt', 'w')
    pickle.dump(arguments, arguments_out)
    arguments_out.close()
    clf = linear_model.Ridge()
    print "learning man"
    print len(description_vectors), len(signature_vectors)
    model = clf.fit(description_vectors, signature_vectors)
    print "learnedman"
    model_out = open('network.dat', 'w')
    pickle.dump(model, model_out)
    print clf.get_params()
    print model


trainNetwork()
