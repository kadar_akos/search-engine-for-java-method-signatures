from gensim import corpora, models, similarities, matutils
import gensim
import numpy
import pickle
import argparse
import os

parser = argparse.ArgumentParser(description='Hello to the vectorspace creator')
parser.add_argument('-data','--data',help='"desps" or "signatures"', required=True)
args = parser.parse_args()

#reads in the text and gives it back line by line
class MyCorpus:
    def __init__(self, name):
        self.name = name
    def __iter__(self):
        for line in open(self.name):
            yield line.lower().split()

#Builds the dictionary from infile and writes it to outfile
#Reads the corpus, creates dictionary, removes stopwords, compactifies the dictionary and returns it + saves it
def getDictionary(infile_name, outfile_name):
    dictionary = corpora.Dictionary(line.split() for line in open(infile_name))
    #dictionary.filter_extremes(no_below=1)
    dictionary.save(outfile_name)
    print dictionary
    return dictionary


def getTFIDFmodel(corpus, dictionary, corpus_out,  features_out):
    mycorpus = MyCorpus(corpus)
    dictionary = corpora.Dictionary.load(dictionary)
    tf = [dictionary.doc2bow(text) for text in mycorpus]
    corpora.MmCorpus.serialize(corpus_out, tf)
    tfidf = models.TfidfModel(tf)
    tfidf_corpus = tfidf[tf]
    feature_vectors = gensim.matutils.corpus2dense(tfidf_corpus, len(dictionary))
    feature_vectors = numpy.transpose(feature_vectors)
    print feature_vectors
    numpy.save(features_out, feature_vectors)
    print 'at similarity'

def getValidFeatures(corpus, dictionary, features_out, similarity_out):
    mycorpus = MyCorpus(corpus)
    dictionary = corpora.Dictionary.load(dictionary)
    num_features = len(dictionary)
    tf = [dictionary.doc2bow(text) for text in mycorpus]
    tfidf = models.TfidfModel(tf)
    tfidf_corpus = tfidf[tf]
    feature_vectors = gensim.matutils.corpus2dense(tfidf_corpus, len(dictionary))
    feature_vectors = numpy.transpose(feature_vectors)
    print feature_vectors
    numpy.save(features_out, feature_vectors)
    getSimilarityIndex(tfidf_corpus, similarity_out+'.index', num_features)



def getSimilarityIndex(corpus, name, numfeatures):
    index = similarities.MatrixSimilarity(corpus, num_features=numfeatures)
    index.save(name)
    if name == 'train_desps_similarity.index':
        arguments_in = open('arguments.txt')
        arguments = pickle.load(arguments_in)
        arguments['num_descs'] = numfeatures
        arguments_out = open('arguments.txt', 'w')
        pickle.dump(arguments, arguments_out)


def getVectorSpaceModel(dataset):
    path = os.getcwd()
    getDictionary(path+'/sets/TrainSet_Desps.txt', path+'/retrieve/train_desps_dict.dict')
    getDictionary(path+'/sets/ALL_SIGNATURES.txt', path+'/retrieve/train_signatures_dict.dict')

    if dataset == 'desps':
        corpus = path+'/sets/TrainSet_Desps.txt'
        corpus_valid = path+'/sets/ValidSet_Desps.txt'
        out_corpus = path+'/retrieve/train_desps_corpus'
        features = path+'/network/train_desps_features'
        features_valid = path+'/network/valid_desps_features'
        dictionary = path+'/retrieve/train_desps_dict.dict'
        index = path+'/retrieve/train_desps_similarity'
        getTFIDFmodel(corpus, dictionary, out_corpus, features)
        getValidFeatures(corpus_valid, dictionary, features_valid, index)


    if dataset == 'signatures':
         corpus = path+'/sets/SIGNATURES_SHORT.txt'
         corpus_valid = path+'/sets/ValidSet_Signatures.txt'
         out_corpus = path+'/retrieve/train_signatures_corpus'
         features = path+'/network/train_signatures_features'
         features_valid = path+'/network/valid_signatures_features'
         index = path+'/retrieve/train_signatures_similarity'
         dictionary = path+'/retrieve/train_signatures_dict.dict'
         getTFIDFmodel(corpus, dictionary, out_corpus, features)
         getValidFeatures(corpus_valid, dictionary, features_valid, index)
         getValidFeatures(path+'/sets/ALL_SIGNATURES.txt', dictionary, 'mindegy', index )


getVectorSpaceModel(args.data)
