from gensim import corpora, similarities
import gensim
import numpy
import os

def getQueryVector(query, num_descs):
    path = os.getcwd()
    corpus = corpora.MmCorpus(path+'/retrieve/train_desps_corpus')
    query = query.lower().split()
    bow_query = dictionary.doc2bow(query)
    tfidf = gensim.models.TfidfModel(corpus, id2word=dictionary)
    tfidf_query = tfidf[bow_query]
    tfidf_query = gensim.matutils.sparse2full(tfidf_query, num_descs)
    query_vector = tfidf_query
    return query_vector


def getSignatureVector(query_vector, network):
    query_vector = numpy.transpose(query_vector)
    signature_vector = network.predict([query_vector])
    signature_vector = gensim.matutils.full2sparse(signature_vector[0])
    return signature_vector


def getQuery(num_query, queries, index):
    description_in = open(queries)
    descriptions = description_in.readlines()
    query = descriptions[num_query]
    index_in = open(index)
    indices = index_in.readlines()
    index = indices[num_query]
    print "INDEX", index
    return query, index


def getPosition(similarity_list, index):
    pos = 0
    for i in similarity_list:
        pos += 1
        if i[0] == int(index):
            return pos
    print pos

def getSimilarityList(signature_vector):
    sims = ind[signature_vector]
    sims = sorted(enumerate(sims), key=lambda item: -item[1])
    return sims


def validate(network, num_descs, num_query, indeces, queries):
    query, index = getQuery(num_query, queries, indeces)
    print "INKABB EZ", index
    query_vector = getQueryVector(query, num_descs)
    signature_vector = getSignatureVector(query_vector, network)
    sims = getSimilarityList(signature_vector)
    pos = getPosition(sims, index)
    print "NUM QUERY", num_query
    return pos

path = os.getcwd()
ind = similarities.MatrixSimilarity.load(path+'/retrieve/train_signatures_similarity.index')
dictionary = corpora.Dictionary.load(path+'/retrieve/train_desps_dict.dict')
