import re, collections, os

#This code is from: http://norvig.com/spell-correct.html

def words(text):
    return re.findall('[a-z]+', text.lower())

def train(features):
    model = collections.defaultdict(lambda: 1)
    for f in features:
        model[f] += 1
    return model
path = os.getcwd()
NWORDS = train(words(file(path+'/sets/Desp_original.txt').read()))

alphabet = 'abcdefghijklmnopqrstuvwxyz'

def edits1(word):
   splits     = [(word[:i], word[i:]) for i in range(len(word) + 1)]
   deletes    = [a + b[1:] for a, b in splits if b]
   transposes = [a + b[1] + b[0] + b[2:] for a, b in splits if len(b)>1]
   replaces   = [a + c + b[1:] for a, b in splits for c in alphabet if b]
   inserts    = [a + c + b     for a, b in splits for c in alphabet]
   return set(deletes + transposes + replaces + inserts)

def known_edits2(word):
    return set(e2 for e1 in edits1(word) for e2 in edits1(e1) if e2 in NWORDS)

def known(words): return set(w for w in words if w in NWORDS)

def correct(query):
    query = query.lower().split()
    corrected_query = ''
    for word in query:
        candidates = known([word]) or known(edits1(word)) or known_edits2(word) or [word]
        corrected_query += max(candidates, key=NWORDS.get) +' '
    return corrected_query

def test():
    queries1 = open(path+'/sets/TestSet_queries.txt').readlines()
    queries2 = open(path+'/sets/ValidSet_queries.txt').readlines()
    queries = queries1+queries2
    couter = 0
    true = 0
    false = 0
    for query in queries:
        query = query.replace('\n', '').strip()
        correc = correct(query).strip()
        if query == correc:
            true += 1
        else:
            false +=1
    print true
    print false
    print float(false)/len(queries)


