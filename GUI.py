from Tkinter import *
import tkMessageBox
from search import getSignatureVector, getSimilarityList
from search import getQueryVector
import pickle
import os
import webbrowser
from spelling import correct


def spelling_corrector(query):
    new_query = correct(query)
    if query.strip() == new_query.strip():
        return query
    else:
        change = tkMessageBox.askyesno('Did you mean?', new_query)
        if change == True:
            query = new_query
        return query

def getMethodList():
    input = method.get()
    if len(sys.argv)>1 and sys.argv[1] == "correct":
        input = spelling_corrector(input)
    query_vector = getQueryVector(input, 6299)
    signature_vector = getSignatureVector(query_vector, network)
    sims = getSimilarityList(signature_vector)
    method_string = resolveMethods(sims)
    new_window(method_string)

def resolveMethods(sims):
    method_string = ''
    counter = 0
    for i in sims[:200]:
        counter += 1
        method_string += str(counter)+'.'+' '+signatures[i[0]].replace('  ',' ').replace('Signature ','').replace(' ', '\n')+'\n'+'Description: '+ desps[i[0]]+'\n\n'
    return method_string

def new_window(sims):
    window = Toplevel(app)
    label = Label(window, text='Results')
    label.pack(side="top", fill="both", padx=10, pady=10)
    scrollbar = Scrollbar(window)
    scrollbar.pack(side=RIGHT, fill=Y)
    listbox = Text(window, yscrollcommand=scrollbar.set, height=400, width=400)
    listbox.insert(END, sims)
    listbox.config(state=DISABLED)
    listbox.pack(side=LEFT, fill=BOTH)
    
    scrollbar.config(command=listbox.yview)

def click_link(event, text):
    webbrowser.open_new(text)

path = os.getcwd()
text_in = open(path+'/sets/Signature_original.txt')
descs_in = open(path+'/sets/Desp_original.txt')
signatures = text_in.readlines()
desps = descs_in.readlines()
model_in = open("network.dat")
network = pickle.load(model_in)

app  = Tk()
app.title('java method search')
app.geometry('750x200+200+200')

labelText = StringVar()
labelText.set("Search for Java method in English")
label1 = Label(app, textvariable=labelText, height=4)
label1.pack()

searchbox = StringVar(None)
method = Entry(app, textvariable=searchbox)
method.pack()

button1 = Button(app, text="Search", width=20, command=getMethodList)
button1.pack(padx=15, pady=15)

url = 'https://bitbucket.org/kadar_akos/search-engine-for-java-method-signatures'
link = Label(text='Link to Bitbucket repo: '+url, foreground="#0000ff")
link.bind("<1>", lambda event, text=url:click_link(event, text))
link.pack()

app.mainloop()